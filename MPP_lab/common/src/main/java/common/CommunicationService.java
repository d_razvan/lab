package common;

import java.util.concurrent.Future;

public interface CommunicationService {
    String SERVER_HOST = "localhost";
    int SERVER_PORT = 1234;

    String ADD_STUDENT = "addStudent";
    String ADD_PROBLEM = "addProblem";
    String ADD_ASSIGNMENT = "addAssignment";
    String ADD_GRADE = "addGrade";
    String REMOVE_STUDENT = "removeStudent";
    String REMOVE_PROBLEM = "removeProblem";
    String REMOVE_ASSIGNMENT = "removeAssignment";
    String UPDATE_STUDENT = "updateStudent";
    String UPDATE_PROBLEM = "updateProblem";
    String UPDATE_ASSIGNMENT = "updateAssignment";
    String GET_STUDENTS = "getStudents";
    String GET_PROBLEMS = "getProblems";
    String GET_ASSIGNMENTS = "getAssignments";

    Future<String> addStudent(long id, String name, int group, String serialNumber);
    Future<String> addProblem(long id, String description);
    Future<String> addAssignment(long id, long sid, long pid);
    Future<String> addGrade(long id, int grade);
    Future<String> removeStudent(long id);
    Future<String> removeProblem(long id);
    Future<String> removeAssignment(long id);
    Future<String> updateStudent(long id, String name, int group, String serialNumber);
    Future<String> updateProblem(long id, String description);
    Future<String> updateAssignment(long id, long sid, long pid);
    Future<String> getStudents();
    Future<String> getProblems();
    Future<String> getAssignments();
}
