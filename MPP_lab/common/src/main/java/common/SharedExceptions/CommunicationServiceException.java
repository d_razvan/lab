package common.SharedExceptions;

public class CommunicationServiceException extends BaseException {
    public CommunicationServiceException(String message) {
        super(message);
    }

    public CommunicationServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommunicationServiceException(Throwable cause) {
        super(cause);
    }
}
