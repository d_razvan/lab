package common.SharedExceptions;

public class InputException extends BaseException {
    public InputException(String message) {
        super(message);
    }
    public InputException(String message, Throwable cause) {
        super(message, cause);
    }

    public InputException(Throwable cause) {
        super(cause);
    }
}