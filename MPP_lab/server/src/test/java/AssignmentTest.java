import org.junit.Before;
import org.junit.Test;
import server.Domain.Assignment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AssignmentTest {

    private Assignment assignment;
    @Before
    public void setUp() throws Exception {
        assignment = new Assignment(1L,2L,10);
    }

    @Test
    public void getStudentID() {
        assertEquals(assignment.getStudentID(),1L);
    }

    @Test
    public void getProblemID() {
        assertEquals(assignment.getProblemID(),2L);
    }

    @Test
    public void getGrade() {
        assertEquals(assignment.getGrade(),10);
    }

    @Test
    public void setStudentID() {
        assignment.setStudentID(3L);
        assertEquals(assignment.getStudentID(),3L);
    }

    @Test
    public void setProblemID() {
        assignment.setProblemID(4L);
        assertEquals(assignment.getProblemID(),4L);
    }

    @Test
    public void setGrade() {
        assignment.setGrade(9);
        assertEquals(assignment.getGrade(),9);
    }

    @Test
    public void equals() {
        Assignment assignment2 = new Assignment(1L,2L,5);
        assertTrue(assignment.equals(assignment2));
    }
}