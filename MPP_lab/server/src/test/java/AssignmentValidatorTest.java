import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import server.Domain.Assignment;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Validators.AssignmentValidator;
import server.Domain.Validators.Validator;

public class AssignmentValidatorTest {
    Validator<Assignment> assignmentValidator;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        assignmentValidator = new AssignmentValidator();
    }

    @Test
    public void validate() {
        Assignment st = new Assignment(1L,2L,10);
        st.setId(1L);
        assignmentValidator.validate(st);
        exception.expect(ValidatorException.class);
        st = new Assignment(-1L,2L,10);
        st.setId(1L);
        assignmentValidator.validate(st);
        exception.expect(ValidatorException.class);
        st = new Assignment(1L,-2L,10);
        st.setId(1L);
        assignmentValidator.validate(st);
        exception.expect(ValidatorException.class);
        st = new Assignment(1L,2L,-1);
        st.setId(1L);
        assignmentValidator.validate(st);
        exception.expect(ValidatorException.class);
        st = new Assignment(1L,2L,1);
        st.setId(-1L);
        assignmentValidator.validate(st);
    }
}