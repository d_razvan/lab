import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Student;
import server.Domain.Validators.StudentValidator;
import server.Domain.Validators.Validator;

public class StudentValidatorTest {

    Validator<Student> studentValidator;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        studentValidator = new StudentValidator();
    }

    @Test
    public void validate() {
        Student st = new Student("name",100,"A");
        studentValidator.validate(st);
        exception.expect(ValidatorException.class);
        st = new Student("name",99,"A");
        studentValidator.validate(st);
    }
}