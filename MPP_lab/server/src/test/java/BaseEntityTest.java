import org.junit.Before;
import org.junit.Test;
import server.Domain.BaseEntity;

import static org.junit.Assert.assertEquals;

public class BaseEntityTest {

    private BaseEntity<Long> base;
    @Before
    public void setUp() throws Exception {
        base = new BaseEntity<>();
        base.setId(0L);
    }

    @Test
    public void getId() {
        assertEquals(base.getId(),new Long(0));
    }

    @Test
    public void setId() {
        base.setId(1L);
        assertEquals(base.getId(),new Long(1));
    }
}