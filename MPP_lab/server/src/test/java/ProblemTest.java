import org.junit.Before;
import org.junit.Test;
import server.Domain.Problem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProblemTest {

    private Problem problem;
    @Before
    public void setUp() throws Exception {
        problem = new Problem("desc");
    }
    @Test
    public void equals() {
        Problem p2 = new Problem("desc");
        assertTrue(problem.equals(p2));
    }

    @Test
    public void getDescription() {
        assertEquals(problem.getDescription(),"desc");
    }

    @Test
    public void setDescription() {
        problem.setDescription("desc2");
        assertEquals(problem.getDescription(),"desc2");
    }
}