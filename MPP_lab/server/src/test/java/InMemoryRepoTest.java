import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Student;
import server.Domain.Validators.StudentValidator;
import server.Domain.Validators.Validator;
import server.Repository.IRepository;
import server.Repository.InMemoryRepo;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;

/**
 * Created by Razvan on 13.03.2018.
 */
public class InMemoryRepoTest {
    @Rule
    public final ExpectedException exception = ExpectedException.none();


    private IRepository<Long, Student> repo;

    @Before
    public void setUp() throws Exception {
        Validator<Student> studentValidator = new StudentValidator();
        this.repo = new InMemoryRepo<>(studentValidator);
    }

    @Test
    public void findOne() throws Exception {
        // BaseEntity<Long> item = new BaseEntity<>();
        Student item = new Student("first",923,"1A2B");
        item.setId(1L);
        this.repo.save(item);
        assertEquals(repo.findOne(1L).get().equals(item),true);
        exception.expect(IllegalArgumentException.class);
        repo.findOne(null);
    }

    @Test
    public void findAll() throws Exception {
        Student st1 = new Student("first",923,"1A2B");
        Student st2 = new Student("second",924,"1A2C");
        st1.setId(1L);
        st2.setId(2L);
        this.repo.save(st1);
        this.repo.save(st2);
        Iterator<Student> its = repo.findAll().iterator();
        int counter = 0;
        while(its.hasNext()){
            its.next();
            counter ++;
        }
        assertEquals(counter,2);


    }

    @Test
    public void save() throws Exception {
        Student st1 = new Student("first",923,"1A2B");
        Student st2 = new Student("second",924,"1A2C");
        Student st3 = new Student("third",920,"1AC2B");
        Student st4 = new Student("fourth",921,"1AD2C");
        st1.setId(1L);
        st2.setId(2L);
        st3.setId(3L);
        st4.setId(4L);

        repo.save(st1);
        repo.save(st2);
        repo.save(st3);
        repo.save(st4);


        assertEquals(repo.findOne(1L).get().equals(st1),true);
        assertEquals(repo.findOne(2L).get().equals(st2),true);
        assertEquals(repo.findOne(3L).get().equals(st3),true);
        assertEquals(repo.findOne(2L).get().equals(st1),false);
        assertEquals(repo.findOne(3L).get().equals(st2),false);

        exception.expect(IllegalArgumentException.class);
        repo.save(null);

        exception.expect(ValidatorException.class);
        st1.setGroup(-1);
        repo.save(st1);
    }

    @Test
    public void delete() throws Exception {
        Student st1 = new Student("first",923,"1A2B");
        Student st2 = new Student("second",924,"1A2C");
        Student st3 = new Student("third",920,"1AC2B");
        Student st4 = new Student("fourth",921,"1AD2C");
        st1.setId(1L);
        st2.setId(2L);
        st3.setId(3L);
        st4.setId(4L);

        repo.save(st1);
        repo.save(st2);
        repo.save(st3);
        repo.save(st4);



        Iterator<Student> its = repo.findAll().iterator();
        int counter = 0;
        while(its.hasNext()){
            its.next();
            counter ++;
        }
        assertEquals(counter,4);


        repo.delete(1L);
        its = repo.findAll().iterator();
        counter = 0;
        while(its.hasNext()){
            its.next();
            counter ++;
        }
        assertEquals(counter,3);

        repo.delete(2L);
        repo.delete(0L);
        its = repo.findAll().iterator();
        counter = 0;
        while(its.hasNext()){
            its.next();
            counter ++;
        }
        assertEquals(counter,2);

        exception.expect(IllegalArgumentException.class);
        repo.delete(null);
    }

    @Test
    public void update() throws Exception {


        Student st3 = new Student("third",920,"1AC2B");
        st3.setId(3L);

        repo.save(st3);

        Student st5 = new Student("updated",923,"1A2B");
        st5.setId(3L);
        repo.update(st5);
        assertEquals(repo.findOne(3L).get().getName(),"updated");

        exception.expect(IllegalArgumentException.class);
        repo.update(null);

        exception.expect(ValidatorException.class);
        st3.setGroup(-1);
        repo.update(st3);

    }

}