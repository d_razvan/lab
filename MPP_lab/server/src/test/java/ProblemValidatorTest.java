import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Problem;
import server.Domain.Validators.ProblemValidator;
import server.Domain.Validators.Validator;

public class ProblemValidatorTest {

    Validator<Problem> problemValidator;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        problemValidator = new ProblemValidator();
    }

    @Test
    public void validate() {
        Problem st = new Problem("name");
        st.setId(1L);
        problemValidator.validate(st);
        exception.expect(ValidatorException.class);
        st.setId(-1L);
        problemValidator.validate(st);
    }
}