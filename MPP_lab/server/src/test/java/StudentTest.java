import org.junit.Before;
import org.junit.Test;
import server.Domain.Student;
import server.Domain.Validators.StudentValidator;
import server.Domain.Validators.Validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StudentTest {

    private Student st;
    @Before
    public void setUp() throws Exception {
        Validator<Student> studentValidator = new StudentValidator();
        st = new Student("Name1",923,"A2BB");
    }


    @Test
    public void getName() {
        assertEquals(st.getName(),"Name1");
    }

    @Test
    public void setName() {
        st.setName("Name2");
        assertEquals(st.getName(),"Name2");
    }

    @Test
    public void getGroup() {
        assertEquals(st.getGroup(),923);
    }

    @Test
    public void setGroup() {
        st.setGroup(924);
        assertEquals(st.getGroup(),924);
    }

    @Test
    public void getSerialNumber() {
        assertEquals(st.getSerialNumber(),"A2BB");
    }

    @Test
    public void setSerialNumber() {
        st.setSerialNumber("A");
        assertEquals(st.getSerialNumber(),"A");
    }

    @Test
    public void equals() {
        Student st2 = new Student("Name1",923,"A2BB");
        assertTrue(st.equals(st2));
    }
}