package server;

import common.CommunicationService;
import common.Message;
import server.Controller.Controller;
import server.Domain.Assignment;
import server.Domain.Exceptions.RepoException;
import server.Domain.Problem;
import server.Domain.Student;
import server.Domain.Validators.AssignmentValidator;
import server.Domain.Validators.ProblemValidator;
import server.Domain.Validators.StudentValidator;
import server.Domain.Validators.Validator;
import server.Repository.*;
import server.Util.Config;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ServerApp {
    public static void main(String args[]) {
        Config conf = new Config();
        Validator<Student> studentValidator = new StudentValidator();
        Validator<Problem> problemValidator = new ProblemValidator();
        Validator<Assignment> assignmentValidator = new AssignmentValidator();
        IRepository<Long, Student> studentRepository = null;
        IRepository<Long, Problem> problemRepository = null;
        IRepository<Long, Assignment> assignmentRepository = null;

        int storageType = conf.getStorageType();
        try {
            switch (storageType) {
                case 0:
                    studentRepository = new InMemoryRepo<>(studentValidator);
                    problemRepository = new InMemoryRepo<>(problemValidator);
                    assignmentRepository = new InMemoryRepo<>(assignmentValidator);
                    break;
                case 1:
                    studentRepository = new StudentFileRepo(studentValidator, conf.getStudentFile());
                    problemRepository = new ProblemFileRepo(problemValidator, conf.getProblemFile());
                    assignmentRepository = new AssignmentsFileRepo(assignmentValidator, conf.getAssignmentFile());
                    break;
                case 2:
                    studentRepository = new StudentXmlRepo(studentValidator, conf.getStudentFile());
                    problemRepository = new ProblemXmlRepo(problemValidator, conf.getProblemFile());
                    assignmentRepository = new AssignmentXmlRepo(assignmentValidator, conf.getAssignmentFile());
                    break;
                case 3:
                    studentRepository = new StudentDBRepo(studentValidator, conf.getUrl(), conf.getUsername(), conf.getPassword());
                    assignmentRepository = new AssignmentDBRepo(assignmentValidator, conf.getUrl(), conf.getUsername(), conf.getPassword());
                    problemRepository = new ProblemDBRepo(problemValidator, conf.getUrl(), conf.getUsername(), conf.getPassword());
                    break;
                default:
                    return;
            }
        }catch(RepoException e){
            e.printStackTrace();
        }

        Controller ctrl = new Controller(studentRepository,problemRepository,assignmentRepository);
        //Console console = new Console(ctrl);
        //console.runConsole();
        runServer(ctrl);
    }

    public static Message createMessage(Future<String> res){
        try{
            String result = res.get();
            return Message.builder()
                    .header(Message.OK)
                    .body(result)
                    .build();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return Message.builder()
                    .header(Message.ERROR)
                    .build();
        }
    }

    public static void runServer(Controller ctrl) {
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        TcpServer tcpServer = new TcpServer(executorService, CommunicationService.SERVER_HOST, CommunicationService.SERVER_PORT);
        CommunicationService communicationService = new CommunicationServiceServer(executorService, ctrl);


        tcpServer.addHandler(CommunicationService.ADD_STUDENT, (request) -> {
            String[] args = request.getBody().split(",");
            Future<String> res = communicationService.addStudent(Long.valueOf(args[0]),
                    args[1], Integer.valueOf(args[2]), args[3]);
            return createMessage(res);
        });

        tcpServer.addHandler(CommunicationService.ADD_PROBLEM, (request) -> {
            String[] args = request.getBody().split(",");
            Future<String> res = communicationService.addProblem(Long.valueOf(args[0]),
                    args[1]);
            return createMessage(res);
        });

        tcpServer.addHandler(CommunicationService.ADD_ASSIGNMENT, (request) -> {
            String[] args = request.getBody().split(",");
            Future<String> res = communicationService.addAssignment(Long.valueOf(args[0]),
                    Long.valueOf(args[1]),Long.valueOf(args[2]));
            return createMessage(res);
        });

        tcpServer.addHandler(CommunicationService.ADD_GRADE, (request) -> {
            String[] args = request.getBody().split(",");
            Future<String> res = communicationService.addGrade(Long.valueOf(args[0]),
                    Integer.valueOf(args[1]));
            return createMessage(res);
        });

        tcpServer.addHandler(CommunicationService.REMOVE_STUDENT, (request) -> {
            Future<String> res = communicationService.removeStudent(Integer.valueOf(request.getBody()));
            return createMessage(res);
        });

        tcpServer.addHandler(CommunicationService.REMOVE_PROBLEM, (request) -> {
            Future<String> res = communicationService.removeProblem(Integer.valueOf(request.getBody()));
            return createMessage(res);
        });

        tcpServer.addHandler(CommunicationService.REMOVE_ASSIGNMENT, (request) -> {
            Future<String> res = communicationService.removeAssignment(Integer.valueOf(request.getBody()));
            return createMessage(res);
        });

        tcpServer.addHandler(CommunicationService.UPDATE_STUDENT, (request) -> {
            String[] args = request.getBody().split(",");
            Future<String> res = communicationService.updateStudent(Long.valueOf(args[0]),
                    args[1], Integer.valueOf(args[2]), args[3]);
            return createMessage(res);
        });

        tcpServer.addHandler(CommunicationService.UPDATE_PROBLEM, (request) -> {
            String[] args = request.getBody().split(",");
            Future<String> res = communicationService.updateProblem(Long.valueOf(args[0]),
                    args[1]);
            return createMessage(res);
        });

        tcpServer.addHandler(CommunicationService.UPDATE_ASSIGNMENT, (request) -> {
            String[] args = request.getBody().split(",");
            Future<String> res = communicationService.updateAssignment(Long.valueOf(args[0]),
                    Long.valueOf(args[1]),Long.valueOf(args[2]));
            return createMessage(res);
        });

        tcpServer.addHandler(CommunicationService.GET_STUDENTS, (request) -> {
            return createMessage( communicationService.getStudents() );
        });

        tcpServer.addHandler(CommunicationService.GET_PROBLEMS, (request) -> {
            return createMessage( communicationService.getProblems() );
        });

        tcpServer.addHandler(CommunicationService.GET_ASSIGNMENTS, (request) -> {
            return createMessage( communicationService.getAssignments() );
        });


        tcpServer.startServer();
    }


}
