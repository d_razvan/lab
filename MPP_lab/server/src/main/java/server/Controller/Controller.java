package server.Controller;

import server.Domain.Assignment;
import server.Domain.Problem;
import server.Domain.Student;
import server.Repository.IRepository;
import com.sun.javafx.binding.SelectBinding;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Controller {
    private IRepository<Long,Student> studentRepo;
    private IRepository<Long, Problem> problemRepo;
    private IRepository<Long, Assignment> assignmentsRepo;

    public Controller(IRepository<Long,Student> sr, IRepository<Long, Problem> pr, IRepository<Long, Assignment> ar){
        this.studentRepo = sr;
        this.problemRepo = pr;
        this.assignmentsRepo = ar;
    }

    public IRepository<Long,Student> getStudentRepo() { return this.studentRepo; }
    public IRepository<Long,Problem> getProblemRepo() { return this.problemRepo; }
    public IRepository<Long, Assignment> getAssignmentsRepo() { return this.assignmentsRepo; }

    public void addStudent(Student student){
        this.studentRepo.save(student);
    }

    public void addProblem(Problem problem){
        this.problemRepo.save(problem);
    }

    public void addAssignment(Assignment assignment) { this.assignmentsRepo.save(assignment); }

    public void deleteStudent(Long studentLong){
        this.studentRepo.delete(studentLong);
    }

    public void deleteProblem(Long problemLong){
        this.problemRepo.delete(problemLong);
    }

    public void deleteAssignment(Long assignmentID) { this.assignmentsRepo.delete(assignmentID); }

    public void updateStudent(Student newStudent){
        this.studentRepo.update(newStudent);
    }

    public void updateProblem(Problem newProblem){ this.problemRepo.update(newProblem); }

    public void updateAssignment(Assignment newAssignment) { this.assignmentsRepo.update(newAssignment); }

    /**
     * Gets all the students from the stundet repository.
     * @return the set of Students in the studentsRepo repository
     */
    public Set<Student> getAllStudents(){
        Iterable<Student> students = this.studentRepo.findAll();
        return StreamSupport.stream(students.spliterator(),false).collect(Collectors.toSet());
    }

    /**
     * Gets all the assignments from the assignments repository
     * @return the set of Assignments in the assignentsRepo repository
     */
    public Set<Assignment> getAllAssignments(){
        Iterable<Assignment> assignments = this.assignmentsRepo.findAll();
        return StreamSupport.stream(assignments.spliterator(),false).collect(Collectors.toSet());
    }

    /**
     * Gets all the problems from the problem repository.
     * @return the set of Problems in the problemRepo repository
     */
    public Set<Problem> getAllProblems(){
        Iterable<Problem> problems = this.problemRepo.findAll();
        return StreamSupport.stream(problems.spliterator(),false).collect(Collectors.toSet());
    }

    /**
     * Filters students by their names. Returns the students for which the name contains the given input string.
     * @param name not null
     * @return the set of Students whose name contains the given parameter
     */

    public Set<Student> filterStudentsByName(String name){
        Iterable<Student> students = studentRepo.findAll();
        Set<Student> filteredStudents= new HashSet<>();
        students.forEach(filteredStudents::add);

        return filteredStudents.stream()
                .filter(n -> n.getName().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toSet());
    }

    /**
     * Filter students by their groups.
     * @param group must be >100 and <999
     * @return the set of Students which belong to the given group
     */
    public Set<Student> filterStudentsByGroup(int group){
        Iterable<Student> students = studentRepo.findAll();
        Set<Student> filteredStudents= new HashSet<>();
        students.forEach(filteredStudents::add);
        return filteredStudents.stream()
                .filter(n -> n.getGroup() == group)
                .collect(Collectors.toSet());
    }





    /**
     * Assigns a grade to an already assigned project.
     * @param id_student must be >0
     * @param id_problem must be >0
     * @param grade default is -1
     * @throws NoSuchElementException when no student with the given id has been found
     */
    public void assignGrade(long assignmentID, int grade) throws NoSuchElementException{
        Optional<Assignment> assignment = this.assignmentsRepo.findOne(assignmentID);
        if( !assignment.isPresent() )
            throw new NoSuchElementException("No assignment with given ID found.");
        Assignment item = assignment.get();
        item.setGrade(grade);
        updateAssignment(item);
    }

    /**
     * Gets the most assigned Problem object by its number of occurencies in the assignments repository.
     * @return a single map entry which maps the problem id with its number of occurencies
     */
    public Map.Entry<Long,Long> getMostAssignedProblem() {
        Iterable<Assignment> assignments = this.assignmentsRepo.findAll();
        Set<Assignment> assignmentsSet = new HashSet<>();
        assignments.forEach(assignmentsSet::add);

        Map<Long,Long> frequencyMap = assignmentsSet.stream()
                        .collect(Collectors.groupingBy(Assignment::getProblemID,
                            Collectors.counting()
                        )
                );

        return frequencyMap.entrySet().stream()
                .max(Comparator.comparingLong(Map.Entry::getValue)).get();

    }


}
