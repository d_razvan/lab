package server.UI;

import server.Controller.Controller;
import server.Domain.Assignment;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Problem;
import server.Domain.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public class Console {
    private Controller ctrl;
    public Console(Controller c){
        this.ctrl = c;
    }

    public void runConsole() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        int option = 0;
        while(true) {
            System.out.println( "\n0 - Exit\n" +
                                "1 - Student add/remove/update/print \n" +
                                "2 - Problem add/remove/update/print \n" +
                                "3 - Assignment add/remove/update/print \n"
            );
            try {
                option = Integer.parseInt( bufferRead.readLine());
                switch(option){
                    case 0:
                        return;
                    case 1:
                        showStudentMenu();
                        break;
                    case 2:
                        showProblemMenu();
                        break;
                    case 3:
                        showAssignmnentMenu();
                        break;

                    default:
                        System.out.println("Option not implemented.");
                        break;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        }
    }










    public void showStudentMenu() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        int option;
        System.out.println( "\n0 - Back \n" +
                "1 - Add student\n" +
                "2 - Remove student\n" +
                "3 - Update student\n" +
                "4 - List students\n" +
                "5 - Filter students\n"
        );
        while(true){
            option = Integer.parseInt( bufferRead.readLine());
            switch(option){
                case 0:
                    return;
                case 1:
                    addStudent();
                    break;
                case 2:
                    removeStudent();
                    break;
                case 3:
                    updateStudent();
                    break;
                case 4:
                    printAllStudents();
                    break;
                case 5:
                    filterStudents();
                    break;
                default:
                    System.out.println("Option not implemented.");
                    break;
            }
        }
    }

    public void addStudent() throws IOException {
        Student student = readStudent();
        if (student == null || student.getId() < 0) {
            return;
        }
        try {
            ctrl.addStudent(student);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
    }

    public void removeStudent() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("ID: ");
        Long id = Long.valueOf(bufferRead.readLine());
        if( id < 0 )
            return;
        ctrl.deleteStudent(id);
    }

    public void updateStudent() throws IOException {
        Student student = readStudent();
        if (student == null || student.getId() < 0)
            return;
        ctrl.updateStudent(student);
    }

    private void printAllStudents() {
        Set<Student> students = ctrl.getAllStudents();
        students.stream().forEach(System.out::println);
    }
    /**
     *
     * @return
     * @throws IOException
     */
    private Student readStudent() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("MyID: ");
        Long id = Long.valueOf(bufferRead.readLine());
        System.out.print("Serial number: ");
        String serialNumber = bufferRead.readLine();
        System.out.print("Name: ");
        String name = bufferRead.readLine();
        System.out.print("Group: ");
        int group = Integer.parseInt(bufferRead.readLine());
        Student student = new Student(name, group, serialNumber);
        student.setId(id);
        return student;

    }

    /**
     * Menu for the filter option.
     * @throws IOException if the given input is not valid
     */
    private void filterStudents() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Filter method: 0 - back, 1 - by name, 2 - by group");
        int choice = Integer.parseInt(bufferRead.readLine());
        switch(choice){
            case 0:
                return;
            case 1:
                filterStudentsByName();
                return;
            case 2:
                filterStudentsByGroup();
                return;
            default:
                System.out.println("Option not implemented");
                return;
        }

    }

    /**
     * Filters students by group based on the given input.
     * @throws IOException if input is not valid
     */
    private void filterStudentsByGroup() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Group: ");
        int group = Integer.parseInt(bufferRead.readLine());
        Set<Student> students = ctrl.filterStudentsByGroup(group);
        students.stream().forEach(System.out::println);

    }

    /**
     * Filters the students based on the given input.
     * @throws IOException if the input is not valid
     */
    private void filterStudentsByName() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Filtering students, name containing...:");
        System.out.print("Part of name: ");
        String name = "";
        name = bufferRead.readLine();
        Set<Student> students = ctrl.filterStudentsByName(name);
        students.stream().forEach(System.out::println);
    }



    public void showProblemMenu() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        int option;
        System.out.println( "\n0 - Back \n" +
                "1 - Add problem\n" +
                "2 - Remove problem\n" +
                "3 - Update problem\n" +
                "4 - List problems\n" +
                "5 - Most assigned problem\n"
        );
        while(true){
            option = Integer.parseInt( bufferRead.readLine());
            switch(option){
                case 0:
                    return;
                case 1:
                    addProblem();
                    break;
                case 2:
                    removeProblem();
                    break;
                case 3:
                    updateProblem();
                    break;
                case 4:
                    printAllProblems();
                    break;
                case 5:
                    printMostAssignedProblem();
                    break;
                default:
                    System.out.println("Option not implemented.");
                    break;
            }
        }
    }

    public void addProblem() throws IOException,ValidatorException {
        Problem problem = readProblem();
        if (problem == null || problem.getId() < 0)
            return;
        ctrl.addProblem(problem);
    }

    public void removeProblem() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("ID: ");
        Long id = Long.valueOf(bufferRead.readLine());
        if( id < 0 )
            return;
        ctrl.deleteProblem(id);
    }

    public void updateProblem() throws IOException {
        Problem problem = readProblem();
        if (problem == null || problem.getId() < 0)
            return;
        ctrl.updateProblem(problem);
    }

    private Problem readProblem() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("ID: ");
        Long id = Long.valueOf(bufferRead.readLine());
        System.out.print("Description: ");
        String description = bufferRead.readLine();

        Problem problem = new Problem(description);
        problem.setId(id);

        return problem;

    }

    private void printAllProblems() {
        Set<Problem> problems = ctrl.getAllProblems();
        problems.stream().forEach(System.out::println);
    }

    private void printMostAssignedProblem(){
        Map.Entry<Long,Long> mostAssig = ctrl.getMostAssignedProblem();
        System.out.println( "Most assigned problem was problem " + mostAssig.getKey() +
        " and it was assigned " + mostAssig.getValue() + " times.");
    }





    public void showAssignmnentMenu() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        int option;
        System.out.println( "\n0 - Back \n" +
                "1 - Add assignment\n" +
                "2 - Remove assignment\n" +
                "3 - Update assignment\n" +
                "4 - List assignments\n" +
                "5 - Assign grade\n"
        );
        while(true){
            option = Integer.parseInt( bufferRead.readLine());
            switch(option){
                case 0:
                    return;
                case 1:
                    addAssignment();
                    break;
                case 2:
                    removeAssignment();
                    break;
                case 3:
                    updateAssignment();
                    break;
                case 4:
                    printAllAssignments();
                    break;
                case 5:
                    assignGrade();
                    break;
                default:
                    System.out.println("Option not implemented.");
                    break;
            }
        }
    }



    public void addAssignment() throws IOException,NoSuchElementException {
        Assignment assignment = readAssignment();
        if ( assignment == null || assignment.getId() < 0 || assignment.getProblemID() < 0)
            return;

        this.ctrl.addAssignment(assignment);
    }

    public void removeAssignment() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("ID: ");
        Long id = Long.valueOf(bufferRead.readLine());
        if( id < 0 )
            return;
        ctrl.deleteAssignment(id);
    }

    public void updateAssignment() throws IOException {
        Assignment assignment = readAssignment();
        if (assignment == null || assignment.getId() < 0)
            return;
        ctrl.updateAssignment(assignment);
    }

    public Assignment readAssignment() throws IOException{
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Assignment ID: ");
        Long assignment_id = Long.valueOf(bufferRead.readLine());
        System.out.print("Student ID: ");
        Long student_id = Long.valueOf(bufferRead.readLine());
        System.out.print("Problem ID: ");
        Long problem_id = Long.valueOf(bufferRead.readLine());
        Assignment assignment = new Assignment(student_id,problem_id);
        assignment.setId(assignment_id);
        return assignment;
    }

    public void assignGrade() throws IOException,NoSuchElementException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Assignment ID: ");
        Long assignmentID = Long.valueOf(bufferRead.readLine());
        System.out.print("Grade: ");
        int grade = Integer.parseInt( bufferRead.readLine());
        this.ctrl.assignGrade(assignmentID,grade);
    }


    private void printAllAssignments() {
        Set<Assignment> assignments = ctrl.getAllAssignments();
        assignments.stream().forEach(System.out::println);
    }
}
