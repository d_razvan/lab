package server;

import common.CommunicationService;
import server.Controller.Controller;
import server.Domain.Assignment;
import server.Domain.Problem;
import server.Domain.Student;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class CommunicationServiceServer implements CommunicationService {
    private ExecutorService executorService;
    private Controller ctrl;

    public CommunicationServiceServer(ExecutorService executorService, Controller ctrl) {
        this.executorService = executorService;
        this.ctrl = ctrl;
    }

    @Override
    public Future<String> addStudent(long id, String name, int group, String serialNumber) {
        Student st = new Student(name,group,serialNumber);
        st.setId(id);
        ctrl.addStudent(st);
        return executorService.submit(() -> "Added Student " + st.toString());
    }

    @Override
    public Future<String> addProblem(long id, String description) {
        Problem p = new Problem(description);
        p.setId(id);
        ctrl.addProblem(p);
        return executorService.submit(() -> "Added Problem " + p.toString());
    }

    @Override
    public Future<String> addAssignment(long id, long sid, long pid) {
        Assignment a = new Assignment(sid,pid);
        a.setId(id);
        ctrl.addAssignment(a);
        return executorService.submit(() -> "Added Assignment("+ a.toString() + ")");
    }

    @Override
    public Future<String> addGrade(long id, int grade) {
        this.ctrl.assignGrade(id,grade);
        return executorService.submit(() -> "Assigned Grade(" + id + "," + grade + ")");
    }

    @Override
    public Future<String> removeStudent(long id) {
        ctrl.deleteStudent(id);
        return executorService.submit(() -> "Removed Student(" + id + ")");
    }

    @Override
    public Future<String> removeProblem(long id) {
        ctrl.deleteProblem(id);
        return executorService.submit(() -> "Removed Problem(" + id + ")");
    }

    @Override
    public Future<String> removeAssignment(long id) {
        ctrl.deleteAssignment(id);
        return executorService.submit(() -> "Remove Assignment(" + id + ")");
    }

    @Override
    public Future<String> updateStudent(long id, String name, int group, String serialNumber) {
        Student st = new Student(name,group,serialNumber);
        st.setId(id);
        ctrl.updateStudent(st);
        return executorService.submit(() -> "Updated Student " + st.toString());
    }

    @Override
    public Future<String> updateProblem(long id, String description) {
        Problem p = new Problem(description);
        p.setId(id);
        ctrl.updateProblem(p);
        return executorService.submit(() -> "Updated Problem " + p.toString());
    }

    @Override
    public Future<String> updateAssignment(long id, long sid, long pid) {
        Assignment a = new Assignment(sid,pid);
        a.setId(id);
        ctrl.updateAssignment(a);
        return executorService.submit(() -> "Updated Assignment("+ a.toString() + ")");
    }

    @Override
    public Future<String> getStudents() {
        String response = ctrl.getAllStudents().stream()
                .map( x -> "[ ID: " + x.getId() + ", Name: " + x.getName() + ", Group: " + x.getGroup() +
                        ", Serial number: " + x.getSerialNumber() + " ]|")
                .reduce((x,y) -> x + y)
                .get();
        return executorService.submit(() -> response);
    }

    @Override
    public Future<String> getProblems() {
        String response = ctrl.getAllProblems().stream()
                .map( x -> "[ ID: " + x.getId() + ", Description: " + x.getDescription() + "]|")
                .reduce((x,y) -> x + y)
                .get();
        return executorService.submit(() -> response);
    }

    @Override
    public Future<String> getAssignments() {
        String response = ctrl.getAllAssignments().stream()
                .map( x -> "[ ID: " + x.getId() + ", Student ID: " + x.getStudentID() + ", Problem ID: " +
                        x.getProblemID() + ", Grade: " + x.getGrade() + "]|")
                .reduce((x,y) -> x + y)
                .get();
        return executorService.submit(() -> response);
    }

}
