package server.Domain.Validators;

import server.Domain.Assignment;
import server.Domain.Exceptions.ValidatorException;

/**
 * Created by Razvan on 18.03.2018.
 */
public class AssignmentValidator implements Validator<Assignment> {
    @Override
    public void validate(Assignment entity) throws ValidatorException {
        if(entity.getStudentID() < 0 || entity.getProblemID() < 0 || entity.getId() < 0)  throw new ValidatorException("ID not in range");
        if(entity.getGrade() < 0 || entity.getGrade() > 100) throw new ValidatorException("Grade not in range [0,100].");
    }
}
