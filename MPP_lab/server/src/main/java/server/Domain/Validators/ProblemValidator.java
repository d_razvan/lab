package server.Domain.Validators;

import server.Domain.Exceptions.ValidatorException;
import server.Domain.Problem;

public class ProblemValidator implements Validator<Problem> {
    @Override
    public void validate(Problem entity) throws ValidatorException {
        if(!((entity.getId()).getClass() ==Long.class)) throw new ValidatorException("Lab problem id not of type \"long\".");
        if(entity.getId() < 0) throw new ValidatorException("ID must be > 0.");

    }
}
