package server.Domain.Validators;

import server.Domain.Exceptions.ValidatorException;
import server.Domain.Student;

public class StudentValidator implements Validator<Student> {
    @Override
    public void validate(Student entity) throws ValidatorException {
        if(entity.getGroup()>999|| entity.getGroup()<100)  throw new ValidatorException("Group not in range");

    }
}
