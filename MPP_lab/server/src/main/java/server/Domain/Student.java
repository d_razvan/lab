package server.Domain;

import java.util.Objects;

public class Student extends BaseEntity<Long> {
    private String name;
    private int group;
    private String serialNumber;

    public Student(String name, int group, String serialNumber) {
        this.name = name;
        this.group = group;
        this.serialNumber = serialNumber;
    }

    /**
     * Getter - Student.name
     * @return string
     */
    public String getName() {
        return name;
    }

    /**
     * Settter - Student.name
     * @param name string
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter - Student.group
     * @return int
     */
    public int getGroup() {
        return group;
    }

    /**
     * Setter - Student.group
     * @param group int
     */
    public void setGroup(int group) {
        this.group = group;
    }


    /**
     * Getter - Student.serialNumber
     * @return string
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Setter - Student.serialNumber
     * @param serialNumber
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", group=" + group +
                ", serialNumber=" + serialNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return group == student.group &&
                Objects.equals(name, student.name) &&
                Objects.equals(serialNumber, student.serialNumber);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, group, serialNumber);
    }
}
