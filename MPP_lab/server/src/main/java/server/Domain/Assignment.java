package server.Domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Razvan on 20.03.2018.
 */
public class Assignment extends BaseEntity<Long> {
    private long studentID;
    private long problemID;
    private int grade;

    public Assignment(){
    }

    public Assignment(long sid, long pid){
        this.studentID = sid;
        this.problemID = pid;
    }

    public Assignment(long sid, long pid, int g){
        this(sid,pid);
        this.grade = g;
    }

    public long getStudentID() { return this.studentID; }
    public long getProblemID() { return this.problemID; }
    public int getGrade() { return this.grade; }
    public void setStudentID(long sid) { this.studentID = sid; }
    public void setProblemID(long pid){ this.problemID = pid; }
    public void setGrade(int g){ this.grade = g; }

    @Override
    public String toString() {
        return "Assignment{" +
                "id=" + getId() +
                ", studentID=" + this.studentID +
                ", problemID=" + this.problemID +
                ", grade=" + this.grade +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Assignment other = (Assignment) o;
        return this.studentID == other.getStudentID() && this.problemID == other.getProblemID();
    }

    @Override
    public int hashCode() {

        return Objects.hash(studentID,problemID);
    }
}
