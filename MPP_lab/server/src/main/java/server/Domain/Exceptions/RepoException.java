package server.Domain.Exceptions;

import common.SharedExceptions.BaseException;

public class RepoException extends BaseException {
    public RepoException(String message) {
        super(message);
    }
    public RepoException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepoException(Throwable cause) {
        super(cause);
    }
}