package server.Domain.Exceptions;

import common.SharedExceptions.BaseException;

/**
 * Created by Razvan on 20.03.2018.
 */
public class DuplicateException extends BaseException {
    public DuplicateException(String message) {
        super(message);
    }
    public DuplicateException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateException(Throwable cause) {
        super(cause);
    }
}
