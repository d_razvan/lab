package server.Domain;

import java.util.Objects;

public class Problem extends BaseEntity <Long>{
    private String description;

    public Problem(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Problem that = (Problem) o;
        return Objects.equals(description, that.description);
    }

    @Override
    public String toString() {
        return "Problem{" +
                "id=" + getId() +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public int hashCode() {

        return Objects.hash(description);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
