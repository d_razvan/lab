package server.Util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Config {
    private int storageType = 0;
    private String studentFile = "";
    private String problemFile = "";
    private String assignmentFile = "";
    private String url = "";
    private String username = "";
    private String password = "";

    public Config(){
        readConfig();
    }

    public void readConfig(){
        Path path = Paths.get(".\\config.ini");
        try {
            List<String> configList = Files.lines(path).collect(Collectors.toList());
            storageType = Integer.valueOf(configList.get(2).substring(28));
            switch(storageType){
                case 1:
                    studentFile = configList.get(6).substring(13);
                    problemFile = configList.get(7).substring(13);
                    assignmentFile = configList.get(8).substring(13);
                    break;
                case 2:
                    studentFile = configList.get(11).substring(12);
                    problemFile = configList.get(12).substring(12);
                    assignmentFile = configList.get(13).substring(12);
                    break;
                case 3:
                    url = configList.get(16).substring(4);
                    username = configList.get(17).substring(5);
                    password = configList.get(18).substring(9);
                    break;
                default:
                    break;
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getStudentFile() { return studentFile; }
    public String getProblemFile() { return problemFile; }
    public String getAssignmentFile() { return assignmentFile; }
    public int getStorageType() { return storageType; }
    public String getUrl() { return url; }
    public String getUsername() { return username; }
    public String getPassword() { return password; }
}
