package server.Util;

import server.Domain.Assignment;
import server.Domain.BaseEntity;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by Razvan on 20.03.2018.
 */
public class XmlWriter<ID, T extends BaseEntity<ID>> {
    private String fileName;

    public XmlWriter(String fileName) {
        this.fileName = fileName;
    }

    public void save(T entity) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
            Document xmlDoc = dbBuilder.parse(this.fileName);
            Element root = xmlDoc.getDocumentElement();
            Element ourBook = xmlDoc.createElement("book");
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }

}
