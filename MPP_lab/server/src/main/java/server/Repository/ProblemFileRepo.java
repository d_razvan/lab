package server.Repository;

import server.Domain.Exceptions.RepoException;
import server.Domain.Problem;
import server.Domain.Validators.Validator;
import server.Domain.Exceptions.ValidatorException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ProblemFileRepo extends InMemoryRepo<Long, Problem> {
    private String fileName;

    public ProblemFileRepo(Validator<Problem> validator, String fileName) {
        super(validator);
        this.fileName = fileName;

        loadData();
    }

    /**
     * Loads data from the specified file.
     * @throws RepoException if any exception has been encountered
     */
    private void loadData() throws RepoException{
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Long id = Long.valueOf(items.get(0));
                String description = items.get(1);

                Problem lab=new Problem(description);
                lab.setId(id);

                try {
                    super.save(lab);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            throw new RepoException(e.getMessage());
        }
    }

    /**
     * Saves a Problem to the repository.
     * @param entity
     *            must not be null.
     * @return an Optional<Problem>
     * @throws ValidatorException if the Problem is not valid
     */
    @Override
    public Optional<Problem> save(Problem entity) throws ValidatorException, RepoException {
        Optional<Problem> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    /**
     * Saves data to the specified file.
     * @param entity - the object to be saved
     * @throws RepoException if any exception has been encountered
     */
    private void saveToFile(Problem entity) throws RepoException{
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId()+","+entity.getDescription());
            bufferedWriter.newLine();
        } catch (Exception e) {
            throw new RepoException(e.getMessage());
        }
    }

}

