package server.Repository;

import server.Domain.Exceptions.RepoException;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Problem;
import server.Domain.Student;
import server.Domain.Validators.Validator;
import server.Util.XmlHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Optional;

public class ProblemXmlRepo extends InMemoryRepo<Long, Problem>{
    private String fileName;

    public ProblemXmlRepo(Validator<Problem> validator, String fileName) {
        super(validator);
        this.fileName = fileName;

        loadData();
    }


    /**
     * Loads data from the specified file.
     * @throws RepoException if any exception has been encountered
     */
    private void loadData() {
        Document doc =  XmlHelper.loadDocument(this.fileName);
        int size = doc.getElementsByTagName("Problem").getLength();
        String id,description;
        Problem p = null;
        for(int i = 0; i < size; i++ ) {
            id = doc.getElementsByTagName("id").item(i).getTextContent();
            description = doc.getElementsByTagName("description").item(i).getTextContent();
            p = new Problem(description);
            p.setId(Long.parseLong(id));
            super.save(p);
        }
    }

    /**
     * Saves a Problem to the repository.
     * @param entity
     *            must not be null.
     * @return an Optional<Problem>
     * @throws ValidatorException if the Problem is not valid
     */
    @Override
    public Optional<Problem> save(Problem entity) throws RepoException{
        Optional<Problem> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    /**
     * Saves data to the specified file.
     * @param entity - the object to be saved
     * @throws RepoException if any exception has been encountered
     */
    private void saveToFile(Problem entity) throws RepoException{
        try
        {
            File myXmlFile=new File(fileName);
            String path=myXmlFile.getAbsolutePath();
            myXmlFile.createNewFile();

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document=documentBuilder.newDocument();

            Element rootElement= document.createElement("Problems");
            document.appendChild(rootElement);
            super.findAll().forEach( problem -> {

                Element problemElement = document.createElement("Problem");
                rootElement.appendChild(problemElement);

                Element id = document.createElement("id");
                id.appendChild(document.createTextNode(""+problem.getId()));
                problemElement.appendChild(id);

                Element description = document.createElement("description");
                description.appendChild(document.createTextNode(problem.getDescription()));
                problemElement.appendChild(description);

            });

            TransformerFactory transformerFactory=TransformerFactory.newInstance();
            Transformer transformer=transformerFactory.newTransformer();
            DOMSource domSource=new DOMSource(document);

            StreamResult streamResult=new StreamResult(path);
            transformer.transform(domSource,streamResult);

        }
        catch (Exception e) {
            throw new RepoException(e.getMessage());
        }

    }
}
