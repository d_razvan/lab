package server.Repository;

import server.Domain.Exceptions.RepoException;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Problem;
import server.Domain.Student;
import server.Domain.Validators.Validator;

import java.sql.*;
import java.util.Optional;

public class ProblemDBRepo extends InMemoryRepo<Long, Problem> {

    private String url, username, password;

    public ProblemDBRepo(Validator<Problem> validator, String url,
                         String username, String password) {
        super(validator);
        this.url = url;
        this.username = username;
        this.password = password;
        loadData();
    }


    /**
     * Loads data from the specified DB.
     * @throws RepoException if any exception has been encountered
     */
    private void loadData() throws RepoException{
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select * FROM \"LabProblems\"");
            Problem p;
            while (rs.next()) {
                p = new Problem(rs.getString(2));
                p.setId(rs.getLong(1));
                super.save(p);
            }
            rs.close();
            st.close();
        } catch (Exception e) {
            throw new RepoException(e.getMessage());
        }


    }


    /**
     * Saves a Problem to the repository.
     * @param entity
     *            must not be null.
     * @return an Optional<Problem>
     * @throws ValidatorException if the Problem is not valid
     */
    @Override
    public Optional<Problem> save(Problem entity) throws ValidatorException {
        Optional<Problem> optional = super.save(entity);

        if (optional.isPresent()) {
            return optional;
        }
        saveToDB(entity);
        return Optional.empty();

    }


    /**
     * Saves data to the specified DB.
     * @param entity - the object to be saved
     * @throws RepoException if any exception has been encountered
     */
    public void saveToDB(Problem entity) throws RepoException{
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement st = conn.createStatement();
            String query = "INSERT INTO \"LabProblems\" VALUES (";
            query += entity.getId() + ",\'" + entity.getDescription() + "\')";
            st.executeUpdate(query);
        } catch (Exception e) {
            throw new RepoException(e.getMessage());
        }

    }
}