package server.Repository;

import server.Domain.Assignment;
import server.Domain.Exceptions.DuplicateException;
import server.Domain.Exceptions.RepoException;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Validators.Validator;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by Razvan on 20.03.2018.
 */
public class AssignmentsFileRepo extends InMemoryRepo<Long, Assignment> {
    private String fileName;

    public AssignmentsFileRepo(Validator<Assignment> validator, String fileName) throws RepoException{
        super(validator);
        this.fileName = fileName;
        loadData();
    }

    /**
     * Loads data from the specified file.
     * @throws RepoException if any exception has been encountered
     */
    private void loadData() throws RepoException{
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Long assignmentID = Long.valueOf(items.get(0));
                Long studentID = Long.valueOf(items.get(1));
                Long problemID = Long.valueOf(items.get(2));
                Integer grade = Integer.valueOf(items.get(3));
                Assignment assignment = new Assignment(studentID,problemID,grade);
                assignment.setId(assignmentID);

                try {
                    super.save(assignment);
                } catch (ValidatorException e) {
                    throw new RepoException(e.getMessage());
                }
            });
        } catch (Exception e) {
            throw new RepoException(e.getMessage());
        }
    }

    /**
     * Saves an Assignment to the repository.
     * @param entity
     *            must not be null.
     * @return an Optional<Assignment>
     * @throws ValidatorException if the Assignment is not valid
     */
    @Override
    public Optional<Assignment> save(Assignment entity) throws ValidatorException, DuplicateException, RepoException {
        Optional<Assignment> optional = super.save(entity);
        if (optional.isPresent())
            throw new DuplicateException("ID already used.");
        saveToFile(entity);
        return Optional.empty();
    }


    /**
     * Saves data to the specified file.
     * @param entity - the object to be saved
     * @throws RepoException if any exception has been encountered
     */
    private void saveToFile(Assignment entity) throws RepoException{
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
            entity.getId() + "," + entity.getStudentID() + "," + entity.getProblemID() + "," + entity.getGrade());
            bufferedWriter.newLine();
        } catch (Exception e) {
            throw new RepoException(e.getMessage());
        }
    }
}
