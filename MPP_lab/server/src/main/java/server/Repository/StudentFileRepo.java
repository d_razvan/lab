package server.Repository;

import server.Domain.Exceptions.RepoException;
import server.Domain.Student;
import server.Domain.Validators.Validator;
import server.Domain.Exceptions.ValidatorException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class StudentFileRepo extends InMemoryRepo<Long,Student> {
    private String fileName;

    public StudentFileRepo(Validator<Student> validator, String fileName) {
        super(validator);
        this.fileName = fileName;
        loadData();
    }

    /**
     * Loads data from the specified file.
     * @throws RepoException if any exception has been encountered
     */
    private void loadData() throws RepoException{
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));
                Long id = Long.valueOf(items.get(0));
                String serialNumber = items.get(1);
                String name = items.get((2));
                int group = Integer.parseInt(items.get(3));

                Student student = new Student( name, group,serialNumber);
                student.setId(id);

                try {
                    super.save(student);
                } catch (ValidatorException e) {
                    throw new RepoException(e.getMessage());
                }
            });
        } catch (Exception e) {
            throw new RepoException(e.getMessage());
        }
    }

    /**
     * Saves a Student to the repository.
     * @param entity
     *            must not be null.
     * @return a Optional<Student>
     * @throws ValidatorException if the Student is not valid
     */
    @Override
    public Optional<Student> save(Student entity) throws ValidatorException, RepoException {
        Optional<Student> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    /**
     * Saves data to the specified file.
     * @param entity - the object to be saved
     * @throws RepoException if any exception has been encountered
     */
    private void saveToFile(Student entity) throws RepoException {
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId() + "," + entity.getSerialNumber() + "," + entity.getName() + "," + entity.getGroup());
            bufferedWriter.newLine();
        } catch (Exception e) {
            throw new RepoException(e.getMessage());
        }
    }
}

