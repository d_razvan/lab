package server.Repository;

import server.Domain.Assignment;
import server.Domain.Assignment;
import server.Domain.Exceptions.RepoException;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Validators.Validator;
import server.Util.XmlHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Optional;

public class AssignmentXmlRepo extends InMemoryRepo<Long, Assignment> {
    private String fileName;

    public AssignmentXmlRepo(Validator<Assignment> validator, String fileName) {
        super(validator);
        this.fileName = fileName;

        loadData();
    }

    /**
     * Loads data from the specified file.
     * @throws RepoException if any exception has been encountered
     */
    private void loadData() {
        Document doc = XmlHelper.loadDocument(this.fileName);
        int size = doc.getElementsByTagName("Assignment").getLength();
        String id, studentID, problemID, grade;
        Assignment p = null;
        for (int i = 0; i < size; i++) {
            id = doc.getElementsByTagName("id").item(i).getTextContent();
            studentID = doc.getElementsByTagName("studentID").item(i).getTextContent();
            problemID = doc.getElementsByTagName("problemID").item(i).getTextContent();
            grade = doc.getElementsByTagName("grade").item(i).getTextContent();
            p = new Assignment(Long.parseLong(studentID), Long.parseLong(problemID), Integer.parseInt(grade));
            p.setId(Long.parseLong(id));
            super.save(p);
        }
    }

    /**
     * Saves an Assignment to the repository.
     * @param entity
     *            must not be null.
     * @return an Optional<Assignment>
     * @throws ValidatorException if the Assignment is not valid
     */
    @Override
    public Optional<Assignment> save(Assignment entity) throws RepoException{
        Optional<Assignment> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }


    /**
     * Saves data to the specified DB.
     * @param entity - the object to be saved
     * @throws RepoException if any exception has been encountered
     */
    private void saveToFile(Assignment entity) throws RepoException{
        try {
            File myXmlFile = new File(fileName);
            String path = myXmlFile.getAbsolutePath();
            myXmlFile.createNewFile();

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element rootElement = document.createElement("Assignments");
            document.appendChild(rootElement);
            super.findAll().forEach(assignment -> {

                Element assignmentElement = document.createElement("Assignment");
                rootElement.appendChild(assignmentElement);

                Element id = document.createElement("id");
                id.appendChild(document.createTextNode("" + assignment.getId()));
                assignmentElement.appendChild(id);

                Element studentID = document.createElement("studentID");
                studentID.appendChild(document.createTextNode("" + assignment.getStudentID()));
                assignmentElement.appendChild(studentID);

                Element problemID = document.createElement("problemID");
                problemID.appendChild(document.createTextNode("" + assignment.getProblemID()));
                assignmentElement.appendChild(problemID);

                Element grade = document.createElement("grade");
                grade.appendChild(document.createTextNode("" + assignment.getGrade()));
                assignmentElement.appendChild(grade);

            });

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);

            StreamResult streamResult = new StreamResult(path);
            transformer.transform(domSource, streamResult);

        } catch (Exception e) {
            throw new RepoException(e.getMessage());
        }

    }
}