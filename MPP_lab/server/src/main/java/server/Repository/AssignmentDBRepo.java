package server.Repository;

import server.Domain.Assignment;
import server.Domain.Exceptions.RepoException;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Problem;
import server.Domain.Validators.Validator;

import java.sql.*;
import java.util.Optional;

public class AssignmentDBRepo extends InMemoryRepo<Long, Assignment> {

    private String url, username, password;

    public AssignmentDBRepo(Validator<Assignment> validator, String url,
                         String username, String password) throws RepoException{
        super(validator);
        this.url = url;
        this.username = username;
        this.password = password;
        loadData();
    }


    /**
     * Loads data from the specified DB.
     * @throws RepoException if any exception has been encountered
     */
    private void loadData() throws RepoException{
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select * FROM \"Assignments\"");
            Assignment a;
            while (rs.next()) {
                a = new Assignment(rs.getLong(2),rs.getLong(3),rs.getInt(4));
                a.setId(rs.getLong(1));
                super.save(a);
            }
            rs.close();
            st.close();
        } catch (Exception e) {
            throw new RepoException(e.getMessage());
        }


    }

    /**
     * Saves an Assignment to the repository.
     * @param entity
     *            must not be null.
     * @return an Optional<Assignment>
     * @throws ValidatorException if the Assignment is not valid
     */
    @Override
    public Optional<Assignment> save(Assignment entity) throws ValidatorException {
        Optional<Assignment> optional = super.save(entity);

        if (optional.isPresent()) {
            return optional;
        }
        saveToDB(entity);
        return Optional.empty();

    }


    /**
     * Saves data to the specified DB.
     * @param entity - the object to be saved
     * @throws RepoException if any exception has been encountered
     */
    public void saveToDB(Assignment entity) throws RepoException{
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement st = conn.createStatement();
            String query = "INSERT INTO \"Assignments\" VALUES (";
            query += entity.getId() + "," + entity.getGrade() + "," + entity.getStudentID() +
                    "," + entity.getProblemID() + ")";
            st.executeUpdate(query);
        } catch (Exception e) {
            throw new RepoException(e.getMessage());
        }
    }
}