package server.Repository;

import server.Domain.Exceptions.RepoException;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Student;
import server.Domain.Validators.Validator;
import server.Util.XmlHelper;
import server.Util.XmlReader;
import server.Util.XmlWriter;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Created by Razvan on 20.03.2018.
 */
public class StudentXmlRepo extends InMemoryRepo<Long, Student> {
    private String fileName;

    public StudentXmlRepo(Validator<Student> validator, String fileName) {
        super(validator);
        this.fileName = fileName;

        loadData();
    }

    /**
     * Loads data from the specified file.
     * @throws RepoException if any exception has been encountered
     */
    private void loadData() {
        Document doc =  XmlHelper.loadDocument(this.fileName);
        int size = doc.getElementsByTagName("Student").getLength();
        String id,name,group,serialNumber;
        Student s = null;
        for(int i = 0; i < size; i++ ) {
            id = doc.getElementsByTagName("id").item(i).getTextContent();
            name = doc.getElementsByTagName("name").item(i).getTextContent();
            group = doc.getElementsByTagName("group").item(i).getTextContent();
            serialNumber = doc.getElementsByTagName("serialNumber").item(i).getTextContent();
            s = new Student(name,Integer.parseInt(group),serialNumber);
            s.setId(Long.parseLong(id));
            super.save(s);
        }
    }

    /**
     * Saves a Student to the repository.
     * @param entity
     *            must not be null.
     * @return a Optional<Student>
     * @throws ValidatorException if the Student is not valid
     */
    @Override
    public Optional<Student> save(Student entity) throws RepoException{
        Optional<Student> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }


    /**
     * Saves data to the specified file.
     * @param entity - the object to be saved
     * @throws RepoException if any exception has been encountered
     */
    private void saveToFile(Student entity) throws RepoException {
        try
        {
            File myXmlFile=new File(fileName);
            String path=myXmlFile.getAbsolutePath();
            myXmlFile.createNewFile();

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document=documentBuilder.newDocument();

            Element rootElement= document.createElement("Students");
            document.appendChild(rootElement);
            super.findAll().forEach( student-> {

                Element studentElement = document.createElement("Student");
                rootElement.appendChild(studentElement);

                Element id = document.createElement("id");
                id.appendChild(document.createTextNode(""+student.getId()));
                studentElement.appendChild(id);

                Element name = document.createElement("name");
                name.appendChild(document.createTextNode(student.getName()));
                studentElement.appendChild(name);

                Element group = document.createElement("group");
                group.appendChild(document.createTextNode("" + student.getGroup()));
                studentElement.appendChild(group);

                Element serialNumber = document.createElement("serialNumber");
                serialNumber.appendChild(document.createTextNode("" + student.getSerialNumber()));
                studentElement.appendChild(serialNumber);
            });

            TransformerFactory transformerFactory=TransformerFactory.newInstance();
            Transformer transformer=transformerFactory.newTransformer();
            DOMSource domSource=new DOMSource(document);

            StreamResult streamResult=new StreamResult(path);
            transformer.transform(domSource,streamResult);

        }
        catch (Exception e) {
            throw new RepoException(e.getMessage());
        }

    }
}
