package server.Repository;

import server.Domain.Exceptions.RepoException;
import server.Domain.Exceptions.ValidatorException;
import server.Domain.Student;
import server.Domain.Validators.Validator;

import java.sql.*;
import java.util.Optional;
import java.lang.StringBuffer;

/**
 * Created by Razvan on 25.03.2018.
 */
public class StudentDBRepo extends InMemoryRepo<Long, Student> {

    private String url, username, password;

    public StudentDBRepo(Validator<Student> validator, String url,
                         String username, String password){
        super(validator);
        this.url = url;
        this.username = username;
        this.password = password;
        loadData();
    }

    /**
     * Loads data from the specified DB.
     * @throws RepoException if any exception has been encountered
     */
    private void loadData() throws RepoException{
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select * FROM \"Students\"");
            Student s;
            while (rs.next()) {
                s = new Student(rs.getString(2),rs.getInt(3),rs.getString(4));
                s.setId(rs.getLong(1));
                super.save(s);
            }
            rs.close();
            st.close();
        }
        catch(Exception e){
            throw new RepoException(e.getMessage());
        }


    }

    /**
     * Saves a Student to the repository.
     * @param entity
     *            must not be null.
     * @return a Optional<Student>
     * @throws ValidatorException if the Student is not valid
     */
    @Override
    public Optional<Student> save(Student entity) throws ValidatorException, RepoException {
        Optional<Student> optional = super.save(entity);

        if (optional.isPresent()) {
            return optional;
        }
        saveToDB(entity);
        return Optional.empty();

    }


    /**
     * Saves data to the specified DB.
     * @param entity - the object to be saved
     * @throws RepoException if any exception has been encountered
     */
    public void saveToDB(Student entity) throws RepoException{
        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement st = conn.createStatement();
            StringBuffer query = new StringBuffer();
            query.append("INSERT INTO \"Students\" VALUES (");
            query.append(entity.getId());
            query.append( ",\'");
            query.append(entity.getName());
            query.append("\',");
            query.append(entity.getGroup());
            query.append(",\'");
            query.append(entity.getSerialNumber());
            query.append("\')");
            st.executeUpdate(query.toString());
        }
        catch(Exception e){
            throw new RepoException(e.getMessage());
        }
    }
}
