package client;

import common.Message;
import common.SharedExceptions.CommunicationServiceException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

public class TcpClient {
    private ExecutorService executorService;
    private String serverHost;
    private int serverPort;

    public TcpClient(ExecutorService executorService, String serverHost, int serverPort) {
        this.executorService = executorService;
        this.serverHost = serverHost;
        this.serverPort = serverPort;

    }

    public Message sendAndReceive(Message request) {
        try (Socket socket = new Socket(serverHost, serverPort);
             OutputStream os = socket.getOutputStream();
             InputStream is = socket.getInputStream();
        ) {
            request.writeTo(os);
            Message response = Message.builder().build();
            response.readFrom(is);
            String body = response.getBody().replace("|","\n");
            System.out.println(body);
            return response;
        } catch (IOException e) {
            e.printStackTrace();
            throw new CommunicationServiceException(e);
        }
    }

}
