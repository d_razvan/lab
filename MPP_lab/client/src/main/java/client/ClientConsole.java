package client;

import common.CommunicationService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.Future;

public class ClientConsole {
    private CommunicationService communicationService;

    CommunicationService hs = null;
    public ClientConsole(CommunicationService communicationService) {
        this.communicationService = communicationService;
    }

    public void runConsole() {
        mainMenu();
    }

    private void mainMenu(){
        while(true) {
            try {
                System.out.println("1 - addStudent, 2 - addProblem, 3 - addAssignment, 4 - assignGrade,\n" +
                        "5 - remStudent, 6 - remProblem, 7 - remAssignment,\n" +
                        "8 - updStudent, 9 - updProblem, 10 - updAssignemnt,\n" +
                        "11 - listStudents, 12 - listProblems, 13 - listAssignments");
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                int choice = Integer.valueOf(bufferRead.readLine());
                Future<String> res = null;
                long id, sid, pid;
                String name, serialNumber, description;
                int grade, group;
                switch (choice) {
                    case 1:
                        System.out.print("ID: ");
                        id = Long.valueOf(bufferRead.readLine());
                        System.out.print("Name: ");
                        name = bufferRead.readLine();
                        System.out.print("Group: ");
                        group = Integer.valueOf(bufferRead.readLine());
                        System.out.print("Serial number: ");
                        serialNumber = bufferRead.readLine();
                        res = communicationService.addStudent(id, name, group, serialNumber);
                        break;
                    case 2:
                        System.out.print("ID: ");
                        id = Long.valueOf(bufferRead.readLine());
                        System.out.print("Description: ");
                        description = bufferRead.readLine();
                        res = communicationService.addProblem(id, description);
                        break;
                    case 3:
                        System.out.print("ID: ");
                        id = Long.valueOf(bufferRead.readLine());
                        System.out.print("Student ID: ");
                        sid = Long.valueOf(bufferRead.readLine());
                        System.out.print("Problem ID: ");
                        pid = Long.valueOf(bufferRead.readLine());
                        res = communicationService.addAssignment(id, sid, pid);
                        break;
                    case 4:
                        System.out.print("ID: ");
                        id = Long.valueOf(bufferRead.readLine());
                        System.out.print("Grade: ");
                        grade = Integer.valueOf(bufferRead.readLine());
                        res = communicationService.addGrade(id, grade);
                        break;
                    case 5:
                        System.out.print("ID: ");
                        id = Long.valueOf(bufferRead.readLine());
                        res = communicationService.removeStudent(id);
                        break;
                    case 6:
                        System.out.print("ID: ");
                        id = Long.valueOf(bufferRead.readLine());
                        res = communicationService.removeProblem(id);
                        break;
                    case 7:
                        System.out.print("ID: ");
                        id = Long.valueOf(bufferRead.readLine());
                        res = communicationService.removeAssignment(id);
                        break;
                    case 8:
                        System.out.print("ID: ");
                        id = Long.valueOf(bufferRead.readLine());
                        System.out.print("Name: ");
                        name = bufferRead.readLine();
                        System.out.print("Group: ");
                        group = Integer.valueOf(bufferRead.readLine());
                        System.out.print("Serial number: ");
                        serialNumber = bufferRead.readLine();
                        res = communicationService.updateStudent(id, name, group, serialNumber);
                        break;
                    case 9:
                        System.out.print("ID: ");
                        id = Long.valueOf(bufferRead.readLine());
                        System.out.print("Description: ");
                        description = bufferRead.readLine();
                        res = communicationService.updateProblem(id, description);
                        break;
                    case 10:
                        System.out.print("ID: ");
                        id = Long.valueOf(bufferRead.readLine());
                        System.out.print("Student ID: ");
                        sid = Long.valueOf(bufferRead.readLine());
                        System.out.print("Problem ID: ");
                        pid = Long.valueOf(bufferRead.readLine());
                        res = communicationService.updateAssignment(id, sid, pid);
                        break;
                    case 11:
                        res = communicationService.getStudents();
                        break;
                    case 12:
                        res = communicationService.getProblems();
                        break;
                    case 13:
                        res = communicationService.getAssignments();
                        break;
                    default:
                        System.out.println("Unrecognized input.");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
