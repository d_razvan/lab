package client;

import common.CommunicationService;
import common.Message;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class CommunicationServiceClient implements CommunicationService {
    private ExecutorService executorService;
    private TcpClient tcpClient;

    public CommunicationServiceClient(ExecutorService executorService, TcpClient tcpClient) {
        this.executorService = executorService;
        this.tcpClient = tcpClient;
    }

    public Future<String> getResponse(String requestType, String requestBody){
        return executorService.submit(() -> {
            Message request = Message.builder()
                    .header(requestType)
                    .body(requestBody)
                    .build();
            Message response = tcpClient.sendAndReceive(request);
            return response.getBody();
        });
    }

    @Override
    public Future<String> addStudent(long id, String name, int group, String serialNumber) {
        return getResponse(CommunicationService.ADD_STUDENT,id+","+name+","+group+","+serialNumber);
    }

    @Override
    public Future<String> addProblem(long id, String description) {
        return getResponse(CommunicationService.ADD_PROBLEM,id+","+description);
    }

    @Override
    public Future<String> addAssignment(long id, long sid, long pid) {
        return getResponse(CommunicationService.ADD_ASSIGNMENT,id+","+sid+","+pid);
    }

    @Override
    public Future<String> addGrade(long id, int grade) {
        return getResponse(CommunicationService.ADD_GRADE,id+","+grade);
    }

    @Override
    public Future<String> removeStudent(long id) {
        return getResponse(CommunicationService.REMOVE_STUDENT,""+id);
    }

    @Override
    public Future<String> removeProblem(long id) {
        return getResponse(CommunicationService.REMOVE_PROBLEM,""+id);
    }

    @Override
    public Future<String> removeAssignment(long id) {
        return getResponse(CommunicationService.REMOVE_ASSIGNMENT,""+id);
    }

    @Override
    public Future<String> updateStudent(long id, String name, int group, String serialNumber) {
        return null;
    }

    @Override
    public Future<String> updateProblem(long id, String description) {
        return null;
    }

    @Override
    public Future<String> updateAssignment(long id, long sid, long pid) {
        return null;
    }

    @Override
    public Future<String> getStudents() {
        return getResponse(CommunicationService.GET_STUDENTS,"");
    }

    @Override
    public Future<String> getProblems() {
        return getResponse(CommunicationService.GET_PROBLEMS,"");
    }

    @Override
    public Future<String> getAssignments() {
        return getResponse(CommunicationService.GET_ASSIGNMENTS,"");
    }

}
