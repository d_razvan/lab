package client;

import common.CommunicationService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ClientApp {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        TcpClient tcpClient = new TcpClient(executorService, CommunicationService.SERVER_HOST, CommunicationService.SERVER_PORT);
        CommunicationService communicationService = new CommunicationServiceClient(executorService, tcpClient);
        ClientConsole clientConsole = new ClientConsole(communicationService);
        clientConsole.runConsole();

        executorService.shutdownNow();
    }
}
